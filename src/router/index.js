import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import TodosComponent from '@/components/TodosComponent'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Router, BootstrapVue)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'TodosComponent',
      component: TodosComponent
    }
  ]
})
